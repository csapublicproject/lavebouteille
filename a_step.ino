#include <Arduino.h>

const int STATUS_ACTIVE = 1;
const int STATUS_ENDED = 2;
const int STATUS_ERROR = 3;

typedef struct Step {
  int id;
  //id de la prochaine étape à activer. 0 si rien
  int (*check)(Step *pStep);

  //activation de l'etape en cours
  void (*activate)(Step *pStep);

  //affichage de la description de l'étape
  String (*display)();

  // ACTIVE, ERROR, ENDED, 
  int status;

  //milli sec du debut de la tache
  unsigned long startTime;

  //milli sec de la fin de la tache
  unsigned long endTime;
  
  //Si erreur ou autre (en fonction du statut), message à afficher
  String msg;
} Step;

Step *STEPS[13];

//On ne fait rien
const int STEP_STOP_ID = 1;

//On purge la machine vers l'exterieur
const int STEP_PURGE_ID = 2;
const int STEP_PURGE_COMPLETE_ID = 3;

//On remplit la cuve
const int STEP_REMPLISSAGE_ID = 4;
const int STEP_REMPLISSAGE_FROID_ID = 5;

//On purge la reserver dans la machine.
const int STEP_REMPLISSAGE_DEPUIS_RESERVE_ID = 6;

//On purge la machine dans la reserve
const int STEP_PURGE_MACHINE_DANS_RESERVE_ID = 7;

//On purge vers l'exterieur suite à cyclage eau chaude car l'eau chaude oxygénée est trop utilisée.
const int STEP_PURGE_MACHINE_SORTIE_ID = 11;

//On fait le cycle eau chaude
const int STEP_CYCLAGE_EAU_CHAUDE_ID = 8;

//On fait le cycle eau froide
const int STEP_CYCLAGE_EAU_FROIDE_ID = 9;

//On attend que quelqu'un vide et remplisse la machine
const int STEP_VIDAGE_REMPLISSAGE_ID = 10;

//Step de test
const int STEP_TEST_ID = 11;

const int STEP_MAX_ID = 13;

/**
 * Variable des différents step
 */
int countUtilisationReserve = 0;
bool isReservePleine = FALSE;

int isEnding(struct Step *pStep) {
  return millis() > pStep->endTime;
}

int checkMaxDuration(unsigned long pMax, struct Step *step) {
  unsigned long delta = millis() - step->startTime;

  return delta > pMax;
}

int checkNothing(Step *pStep) {
  return 0;
}

void activateNothing(Step *pStep) {
}

void setErrorMsg(Step *pStep,String pMsg) {
     pStep->status = STATUS_ERROR;
     pStep->msg = pMsg;
}

void setMsg(Step *pStep,String pMsg) {
     pStep->msg = pMsg;
}
