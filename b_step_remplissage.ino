
Step REMPLISSAGE = {STEP_REMPLISSAGE_ID,&checkRemplissage,&activateRemplissage, &displayRemplissage};
Step REMPLISSAGE_FROID = {STEP_REMPLISSAGE_FROID_ID,&checkRemplissageAFroid,&activateRemplissageAFroid, &displayRemplissageFroid};
Step REMPLISSAGE_DEPUIS_RESERVE = {STEP_REMPLISSAGE_DEPUIS_RESERVE_ID,&checkPurgeReserve,&activatePurgeReserve, &displayPurgeReserve};

int checkRemplissage(Step *pStep) {
  if (isEnding(pStep)) {
    setErrorMsg(pStep,F("Trop long de remplissage")); 
    return 0;
  }

  if (isPlein()) {
//      Serial.print(F("Remplissage Plein : Hum="));
//      Serial.println(valeurHumidite);
      return STEP_CYCLAGE_EAU_CHAUDE_ID;
  }
  
  return 0;
}

void activateRemplissage(Step *pStep) {
  pStep->endTime = pStep->startTime + ERR_MAX_TEMP_REMPLISSAGE;
  digitalWrite(PIN_OUT_VALVE_ARRIVEE, HIGH);
}


String displayRemplissage() {
    return F("Remplissage");
}

int checkRemplissageAFroid(Step *pStep) {
  if (isEnding(&REMPLISSAGE_FROID)) {
    setErrorMsg(pStep,F("Trop long de remplissage")); 
    return 0;
  }
  
  if (isPlein()) {
//      Serial.print(F("Remplissage Froid Plein : Hum="));
//      Serial.println(valeurHumidite);
      return STEP_CYCLAGE_EAU_FROIDE_ID;      
  }

  return 0;
}

void activateRemplissageAFroid(Step *pStep) {
  REMPLISSAGE_FROID.endTime = REMPLISSAGE_FROID.startTime + ERR_MAX_TEMP_REMPLISSAGE;
  digitalWrite(PIN_OUT_VALVE_ARRIVEE, HIGH);
}


String displayRemplissageFroid() {
    return F("Remp. froid");
}

int checkPurgeReserve(Step *pStep) {  
  if (isEnding(&REMPLISSAGE_DEPUIS_RESERVE)) {
    return STEP_CYCLAGE_EAU_CHAUDE_ID;
  }
  return 0;
}

void activatePurgeReserve(Step *pStep) {
  countUtilisationReserve = countUtilisationReserve + 1;
  isReservePleine = FALSE;
  digitalWrite(PIN_OUT_POMPE_EXTERNE, HIGH);
  REMPLISSAGE_DEPUIS_RESERVE.endTime = REMPLISSAGE_DEPUIS_RESERVE.startTime + MAX_TEMP_PURGE_RESERVE;
}

String displayPurgeReserve() {
    return F("Reserve->");
}
