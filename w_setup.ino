#include <Arduino.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2);

Step *DEFAULT_STEP = &REMPLISSAGE;


Step *current = NULL;

Step *first = DEFAULT_STEP;
//Step *first = &CYCLAGE_EAU_CHAUDE;
//Step *first = &CYCLAGE_EAU_FROIDE;
//Step *first = &PURGE;
//Step *first = &PURGE_MACHINE_DANS_RESERVE;
//Step *first = &PURGE_MACHINE_SORTIE;
//Step *first = &PURGE_COMPLETE;
//Step *first = &REMPLISSAGE;
//Step *first = &REMPLISSAGE_FROID;
//Step *first = &REMPLISSAGE_DEPUIS_RESERVE;
//Step *first = &STOP;
//Step *first = &VIDAGE_REMPLISSAGE;
//Step *first = &STEP_TEST;

void setup() {
  analogReference(INTERNAL);
  
  Serial.begin(9600);
  lcd.init(); // initialisation de l'afficheur
  lcd.backlight();

  setupOutput();
  Serial.println(F("setupOutput"));

  setupSensor();
  Serial.println(F("setupSensor"));

  resetOutput();
  Serial.println(F("resetOutput"));

  Serial.println(F("Activation Current Step"));
  activateStep(first);

  STEPS[VIDAGE_REMPLISSAGE.id] = &VIDAGE_REMPLISSAGE;
  STEPS[STOP.id] = &STOP;

  STEPS[CYCLAGE_EAU_CHAUDE.id] = &CYCLAGE_EAU_CHAUDE;
  STEPS[CYCLAGE_EAU_FROIDE.id] = &CYCLAGE_EAU_FROIDE;

  STEPS[REMPLISSAGE.id] = &REMPLISSAGE;
  STEPS[REMPLISSAGE_FROID.id] = &REMPLISSAGE_FROID;
  STEPS[REMPLISSAGE_DEPUIS_RESERVE.id] = &REMPLISSAGE_DEPUIS_RESERVE;

  STEPS[PURGE.id] = &PURGE;
  STEPS[PURGE_MACHINE_DANS_RESERVE.id] = &PURGE_MACHINE_DANS_RESERVE;
  STEPS[PURGE_COMPLETE.id] = &PURGE_COMPLETE;
  STEPS[PURGE_MACHINE_SORTIE.id] = &PURGE_MACHINE_SORTIE;
  STEPS[STEP_TEST.id] = &STEP_TEST;

//  pADC.calibrateBG();
}

void activateStep(Step *pStep) {
    if (current != NULL) {
      Serial.print(F("A partir de "));
      Serial.println(current->display());
      Serial.print(F("Activation du step "));
      Serial.println(pStep->display());
  
      current->status = STATUS_ENDED;
    }
    
    current = pStep;
    current->status = STATUS_ACTIVE;
    current->startTime = millis();
    current->endTime = 0;
    resetOutput();
    current->activate(current);
}
