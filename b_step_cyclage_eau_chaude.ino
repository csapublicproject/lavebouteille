
Step CYCLAGE_EAU_CHAUDE = {STEP_CYCLAGE_EAU_CHAUDE_ID,&checkCyclageEauChaude,&activateCyclageEauChaude, &displayCEC};
Step CYCLAGE_EAU_FROIDE = {STEP_CYCLAGE_EAU_FROIDE_ID,&checkCyclageEauFroide,&activateCyclageEauFroide, &displayCEF};

//Indique sur le step en cours si l'eau chaude est atteinte
int eauChaudeAtteinte = 0;

//Indique si en ce moment, ca chauffe ou non
bool caChauffe = 0;

int checkCyclageEauChaude(Step *pStep) {
  regulationEauChaude();

  if (eauChaudeAtteinte) {
    unsigned long delta = millis() - pStep->endTime;
  
    if (delta > 0) {
      if (countUtilisationReserve == MAX_COUNT_UTILISATION_RESERVE) {
//        setMsg(F("Reuse eau oxy"))
        countUtilisationReserve = 0;
        return STEP_PURGE_MACHINE_SORTIE_ID;
      } else {
//        setMsg(F("On recycle l'eau oxygènée"))
        return STEP_PURGE_MACHINE_DANS_RESERVE_ID;
      }
    }
  }

  return 0;
}

void activateCyclageEauChaude(Step *pStep) {

  if (!isPlein()) {
     setErrorMsg(pStep,F("Machine pas pleine")); 
  } else {
    digitalWrite(PIN_OUT_CYCLAGE, HIGH);
    eauChaudeAtteinte = 0;
  }
}

void regulationEauChaude() {
  if (getTemperatureEau() > TEMP_EAU_CHAUDE) {
      digitalWrite(PIN_OUT_RESISTANCE, LOW);
      caChauffe = 0;
      
      if (!eauChaudeAtteinte) {
        Serial.print(F("Eau Chaude Atteinte"));

        CYCLAGE_EAU_CHAUDE.endTime = millis() + MAX_TEMP_CYCLAGE_CHAUD;
        eauChaudeAtteinte = TRUE;
      }
  } else if (getTemperatureEau() < (TEMP_EAU_CHAUDE - 1)) {
      if (!caChauffe) {
        Serial.print(F("Declenchement de la chauffe"));
      }
      digitalWrite(PIN_OUT_RESISTANCE, HIGH);
      caChauffe = 1;
  }
}

String displayCEC() {
  if (eauChaudeAtteinte) {
    return F("Lavage");
  } else {
    return F("Chauffage");
  }
}


int checkCyclageEauFroide(Step *pStep) {
/*  
  if (!isPlein()) {
     setErrorMsg(pStep,F("Machine pas pleine")); 
     return 0;
  }
*/

  if (isEnding(pStep)) {
    return STEP_PURGE_ID;
  }
  return 0;
}

void activateCyclageEauFroide(Step *pStep) {
  digitalWrite(PIN_OUT_CYCLAGE, HIGH);
  CYCLAGE_EAU_FROIDE.endTime = CYCLAGE_EAU_FROIDE.startTime + MAX_TEMP_CYCLAGE_FROID;
}

String displayCEF() {
    return F("Lavage Froid");
}
