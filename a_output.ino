#include <Arduino.h>

int PIN_OUT_VALVE_SORTIE = 2;
int PIN_OUT_POMPE_EXTERNE = 3;
int PIN_OUT_VALVE_RESERVE = 4;
int PIN_OUT_VALVE_ARRIVEE = 5;
int PIN_OUT_CYCLAGE = 6;
int PIN_OUT_PURGE = 7;
int PIN_OUT_RESISTANCE = 8;


void setupOutput() {
  Serial.println(F("Setup Output"));

  pinMode(PIN_OUT_CYCLAGE, OUTPUT);
  pinMode(PIN_OUT_PURGE, OUTPUT);
  pinMode(PIN_OUT_RESISTANCE, OUTPUT);
  pinMode(PIN_OUT_VALVE_ARRIVEE, OUTPUT);
  pinMode(PIN_OUT_VALVE_RESERVE, OUTPUT);
  pinMode(PIN_OUT_VALVE_SORTIE, OUTPUT);
  pinMode(PIN_OUT_POMPE_EXTERNE, OUTPUT);
}

void resetOutput() {
  Serial.println(F("Reset Output"));

  digitalWrite(PIN_OUT_CYCLAGE, LOW);
  digitalWrite(PIN_OUT_RESISTANCE, LOW);
  digitalWrite(PIN_OUT_VALVE_SORTIE, LOW);
  digitalWrite(PIN_OUT_VALVE_ARRIVEE, LOW);
  digitalWrite(PIN_OUT_VALVE_RESERVE, LOW);
  digitalWrite(PIN_OUT_POMPE_EXTERNE, LOW);
  digitalWrite(PIN_OUT_PURGE, LOW);
}
