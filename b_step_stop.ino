
Step STOP = {STEP_STOP_ID,&checkNothing,&activateNothing,&displayStop};
Step VIDAGE_REMPLISSAGE = {STEP_VIDAGE_REMPLISSAGE_ID,&checkVidage,&activateNothing, &displayVidage};
Step STEP_TEST = {STEP_TEST_ID,&checkNothing,&activateTest, &displayTest};

int checkVidage(Step *pStep) {
  //Si on fait un appui comme pour ouvrir la machine, on redemare un cycle de lavage
  toggleStatusOuvert();
  if (isReservePleine) {
    return STEP_REMPLISSAGE_DEPUIS_RESERVE_ID;
  } else {
    return STEP_REMPLISSAGE_ID;
  } 
  return 0;
}

String displayVidage() {
    return F("Vid.->Rempl.");
}

String displayStop() {
    return F("Arret");
}

String displayTest() {
    return F("Test");
}

void activateTest(Step *pStep) {
//  digitalWrite(PIN_OUT_CYCLAGE, HIGH);
//  digitalWrite(PIN_OUT_RESISTANCE, HIGH);
//  digitalWrite(PIN_OUT_VALVE_ARRIVEE, HIGH);
  digitalWrite(PIN_OUT_VALVE_RESERVE, HIGH);
//  digitalWrite(PIN_OUT_VALVE_SORTIE, HIGH);

//  digitalWrite(PIN_OUT_POMPE_EXTERNE, HIGH);
//  digitalWrite(PIN_OUT_PURGE, HIGH);
}
