#include <Arduino.h>

unsigned long lastDisplayTime;

bool stopState = FALSE;
bool stopErrorState = FALSE;
unsigned long stopBegin = 0;
int msgDisplayCounter = 0;

void loop()
{    
  aquire();

  if (current->id == STEP_VIDAGE_REMPLISSAGE_ID) {
    if (stateArretViaBouton) {
      int nextStep = current->check(current);
      
      if (nextStep)
      {
        activate(nextStep);
      }
    }
  } else {
    if (processButtonChange()) {
      if (current->status != STATUS_ERROR) {
        int nextStep = current->check(current);
        
        if (nextStep)
        {
          //Attendre 1 sec pour avoir le temps de fermer la porte !
          delay(1000);
          activate(nextStep);
        }
      } else {
        if (!stopErrorState) {
          stopErrorState = TRUE;
          Serial.println(F("Stop du step en cours via erreur"));
          resetOutput();
        }
      }
    }
  }

  if (doDisplay()) {
    displaySensor();
//    displayDebug();
    displayError();
    displayLcd();
  }

  delay(MS_DELAY);
}

/*
 * Process button event change
 * return TRUE if the normal process (state check and activate) should be followed
 */
bool processButtonChange() {
  if (stateArretCompletViaBouton) {
    //Appui de 3 sec pour la purge complet ou repartir avec le defautl state
    if (current->id == STEP_STOP_ID) {
      activateStep(DEFAULT_STEP);
    } else {
      activate(STEP_PURGE_COMPLETE_ID);
    }
    stateArretCompletViaBouton = FALSE;
    return FALSE;
  } else if (stateArretViaBouton) {
    //Si le bouton a été appuié puis relaché, on arrête tout
    if (stopState == FALSE) {
      Serial.println(F("Stop du step en cours via bouton"));
      resetOutput();
      stopState = TRUE;
      stopBegin = millis();
      displayLcd();
    }
    return FALSE;
  } else {
    if (stopErrorState && stopState) {
        Serial.println(F("Reprise impossible du step en cours via bouton car Erreur"));
        stopState = FALSE;
        displayLcd();
        return FALSE;
    } else {
      //Si cela vient de changer, on arrête redemare.
      if (stopState) {
        Serial.println(F("Reprise du step en cours via bouton"));
        current->activate(current);
        stopState = FALSE;
        if (current->endTime != 0) {
          current->endTime += millis() - stopBegin;
        }
        stopBegin = 0;
        displayLcd();
        return FALSE;
      }  
    }
  }
  
  return TRUE;
}

int doDisplay() {
  unsigned long now = millis();
  int delayDisplay = 980;

  if (stopState) {
    delayDisplay = 10000;
  }

  if (now > lastDisplayTime + delayDisplay) {
    lastDisplayTime = now;
    return TRUE;
  }

  return FALSE;
}
void activate(int nextStepId) {
  activateStep(STEPS[nextStepId]);
}

/**
   SECTION DISPLAY
*/

  /*
  Max : 8 car       20°C
  <etape desc | ouvert>    <Temp>
  <durée restante>
  16 x 2

  Témpérature
  etape
  Durée restante de l'étape
  ouvert ou non
  Combien de cycle eau chaude
  Msg d'erreur :
  Msg tout court : pour appui bouton
  Si boutton 1, B en position 15,1
*/
void displayLcd() {
  //Premiere ligne
  if (stopState) {
    lcd.setCursor(0, 0);
    lcd.print(F("Arret Bouton"));
  } else {
    lcd.setCursor(0, 0);
    lcd.print(F("            "));
    lcd.setCursor(0, 0);
    lcd.print(current->display());
  }

  lcd.setCursor(13, 0);
  lcd.print(getTemperatureEau(), 1);
  lcd.setCursor(15, 0);
  lcd.print("C");

  //Deuxième ligne
  if (current->status == STATUS_ERROR) {
    lcd.setCursor(0, 1);
    lcd.print(F("E:"));

    lcd.setCursor(2, 1);
    lcd.print(current->msg);
  } else if (current->msg.length() != 0) {
    
    lcd.setCursor(0, 1);
    lcd.print(F("I:"));

    lcd.setCursor(2, 1);
    lcd.print(current->msg);

    msgDisplayCounter++;

    if (msgDisplayCounter > 10) {
      msgDisplayCounter = 0;
      current->msg = F("");
      Serial.println("Arrete le msg d'info apres 10 cycles");
    }
    
  } else if (current->endTime != 0) {
      displayLcdCounter();
  } 

  if (isPlein()) {
    lcd.setCursor(14, 1);
    lcd.print(F("P"));
  } else {
    lcd.setCursor(14, 1);
    lcd.print(F(" "));
  }

  if (isButton()) {
    lcd.setCursor(15, 1);
    lcd.print(F("B"));
  } else {
    lcd.setCursor(15, 1);
    lcd.print(F(" "));
  }
}

void displayHumidite()
{
  Serial.print("Humidite=");
  Serial.print(valeurHumidite);

  lcd.setCursor(6, 1);

  if (isVide()) {
    lcd.print("Vide   ");
  } else if (isPlein()) {
    lcd.print("Plein  ");
  } else {
    lcd.print("-");
  }
}

void displayDebug() {
/*  Serial.print("Vcc: ");
            Serial.print(pADC.readVcc());
            Serial.println("mV");
  Serial.println("");
  Serial.print("DEBUG :");
  Serial.print(current->id);
  Serial.print(" endTime=");
  Serial.println(current->endTime);

  Serial.print("DEBUGCSA AGAIN ");
  //    Serial.println(&CYCLAGE_EAU_CHAUDE);
  Serial.println(CYCLAGE_EAU_FROIDE.endTime);
  */
}

void displayError() {
  if (current->status == STATUS_ERROR) {
      Serial.println("Erreur pour l'étape " + current->display());
      Serial.println("Erreur STATUT: " + current->status);
      Serial.println("Erreur Msg: " + current->msg);
  }
}

void displayLcdCounter() {
    unsigned long now = millis();
    
    if (stopState || current->endTime == 0) {
        lcd.setCursor(0, 1);
        lcd.print(F("     "));      
      //Fait rien : le compteur de temps ne s'affiche plus car il sera remis à jour lors de la remise en marche
    } else if (current->endTime > now) {
        int secTotal = (current->endTime - now) / 1000;
        int sec = secTotal % 60;
        int min = secTotal / 60;
        
/*        Serial.print("TESTCSA debug delta: ");
        Serial.println(secTotal);
        Serial.print("TESTCSA debug sec: ");
        Serial.println(sec);
        Serial.print("TESTCSA debug min: ");
        Serial.println(min);
  */      
        displayLcdFigure(sec, 3);
        lcd.setCursor(2, 1);
        lcd.print(F(":"));
        displayLcdFigure(min, 0);

//      char buffer[2];
//      snprintf(buffer, sizeof(buffer), "%02d", delta / 60);
    } else {
        lcd.setCursor(0, 1);
        lcd.print(F("End  "));
    }
}

void displayLcdFigure(int i, int initialPos) {
     if (i < 10) {
       lcd.setCursor(initialPos, 1); 
       lcd.print(F("0"));
       lcd.setCursor(initialPos + 1, 1); 
       lcd.print(i);
     } else {
       lcd.setCursor(initialPos, 1); 
       lcd.print(i);
     }
}
