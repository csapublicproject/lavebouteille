#include <Arduino.h>
//#include "PrecisionADC.h"

const int PIN_IN_BUTTON = 9;

const int PIN_IN_LIGHT = A0;
const int PIN_IN_TEMP = A1;
const int PIN_IN_HUMIDITE = A2;

const int PIN_IN_VX = A3;
const int PIN_IN_VY = A4;

int valeurTemperature = 0;
int valeurHumidite = 0;
int luminosite = 0;
int nbCyclechgtEtatBoutton = 0;
int button = 0;

// 0 la machine continue normalement/ 1 la machine est arretée
int stateArretViaBouton = 0;
int stateArretCompletViaBouton = 0;

const int NIVEAU_PLEIN = 450;
const int NIVEAU_VIDE = 150;

const float A_TEMP = -0.008837209;
const float B_TEMP = 71.4883;
const float R_TEMP = 975;

//PrecisionADC pADC;

struct Sensor {
  int id;
};

void  setupSensor() {
  Serial.println("Setup Sensor");

  pinMode(PIN_IN_LIGHT, INPUT);
  pinMode(PIN_IN_TEMP, INPUT);
  pinMode(PIN_IN_HUMIDITE, INPUT);
  /*
    pinMode(PIN_IN_VX, INPUT);
    pinMode(PIN_IN_VY, INPUT);
  */
  pinMode(PIN_IN_BUTTON, INPUT_PULLUP);
}

void displaySensor() {
  Serial.print(F("#"));
  Serial.print(F("T="));
  Serial.print(valeurTemperature);
  Serial.print(F(" TReel="));
  Serial.print(getTemperatureEau());
  Serial.print(F("/ Hum="));
  Serial.print(valeurHumidite);
  Serial.print(F("/ lum="));
  Serial.print(luminosite);
  Serial.print(F("/ but="));
  Serial.print(button);
  Serial.print(F("/ mem="));
  Serial.println(freeMemory());
}

void aquire() {
  valeurTemperature = analogRead(PIN_IN_TEMP);
//  valeurTemperature = pADC.analogVoltage(PIN_IN_TEMP);

  int auxValeurHumidite = analogRead(PIN_IN_HUMIDITE);
//  int auxValeurHumidite = pADC.analogVoltage(PIN_IN_HUMIDITE);


  //Svt, la valeur lue est zéro. Du coup j'ignore qd c'est zéro.
  if (auxValeurHumidite != 0) {
    valeurHumidite = auxValeurHumidite;
  }

  luminosite = analogRead(PIN_IN_LIGHT);

  // Zero when the button is push
  button = digitalRead(PIN_IN_BUTTON);

  if (button == 0) {
      nbCyclechgtEtatBoutton++;

      if (nbCyclechgtEtatBoutton > NB_CYCLE_ARRET_COMPLET) {
        stateArretCompletViaBouton = TRUE;
        stateArretViaBouton = FALSE;
        //Pour eviter qu'on detecte un arret bouton apres la détection de l'arret complet
        nbCyclechgtEtatBoutton = 0;
      }
  } else {
      if (nbCyclechgtEtatBoutton > 0) {
        toggleStatusOuvert();
      }
  }
}

int toggleStatusOuvert() {
        nbCyclechgtEtatBoutton = 0;
        stateArretViaBouton = !stateArretViaBouton;
        stateArretCompletViaBouton = FALSE;
}

int getTemperatureEau() {
  // 63°C -> 497
  // 20°C -> 878
  float t = A_TEMP * R_TEMP * (float)valeurTemperature / (1024.0 - (float)valeurTemperature) + B_TEMP;
  return (int) t;
}


/***************************/
/* SECTION HUMIDITE        */

int isPlein() {
  return valeurHumidite > NIVEAU_PLEIN;
}

int isVide() {
  return valeurHumidite < NIVEAU_VIDE;
}

int isOuvert() {
  return stateArretViaBouton;
}

/**
   SECTION BUTTON
*/
int isButton() {
  return button == 0;
}
