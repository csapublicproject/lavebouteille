
Step PURGE = {STEP_PURGE_ID,&checkPurge,&activatePurge, &displayPurge};
Step PURGE_MACHINE_DANS_RESERVE = {STEP_PURGE_MACHINE_DANS_RESERVE_ID,&checkPurgeMachineDansReserve,&activatePurgeMachineDansReserve, &displayPurgeMachineDansReserve};
Step PURGE_MACHINE_SORTIE = {STEP_PURGE_MACHINE_SORTIE_ID,&checkPurgeMachineDansReserve,&activatePurge, &displayPurgeMachineSortie};
Step PURGE_COMPLETE = {STEP_PURGE_COMPLETE_ID,&checkPurgeComplete,&activatePurgeComplete, &displayPurgeComplete};

int checkPurge(Step *pStep) {
  if (isEnding(pStep)) {
    return STEP_VIDAGE_REMPLISSAGE_ID;
  }

  return 0;
}

void activatePurge(Step *pStep) {
  pStep->endTime = pStep->startTime + MAX_TEMP_PURGE;
  digitalWrite(PIN_OUT_VALVE_SORTIE, HIGH);
  digitalWrite(PIN_OUT_PURGE, HIGH);
}

String displayPurge() {
    return F("Purge");
}

/*
int checkPurge(Step *pStep) {
  if (isVide()) {
      Serial.print(F("Purge Vers Reserve Vide : Hum="));
      Serial.println(valeurHumidite);
      return STEP_VIDAGE_REMPLISSAGE_ID;
  }
 
  
  if (isEnding(pStep)) {
    return STEP_VIDAGE_REMPLISSAGE_ID;
  }
  return 0;
}

void activatePurge(Step *pStep) {
  digitalWrite(PIN_OUT_VALVE_SORTIE, HIGH);
  digitalWrite(PIN_OUT_PURGE, HIGH);
  pStep->endTime = pStep->startTime + MAX_TEMP_PURGE;
}

String displayPurge() {
    return "Purge";
}
*/
int checkPurgeComplete(Step *pStep) {
  if (isEnding(pStep)) {
    return STEP_STOP_ID;
  }
  return 0;
}

void activatePurgeComplete(Step *pStep) {
  digitalWrite(PIN_OUT_VALVE_SORTIE, HIGH);
  digitalWrite(PIN_OUT_PURGE, HIGH);
  digitalWrite(PIN_OUT_POMPE_EXTERNE, HIGH);
  pStep->endTime = pStep->startTime + MAX_TEMP_PURGE_COMPLETE;
}

String displayPurgeComplete() {
    return F("Purge Full");
}

int checkPurgeMachineDansReserve(Step *pStep) {
  if (isEnding(pStep)) {
    return STEP_REMPLISSAGE_FROID_ID;
  }

/*
  if (isVide()) {
      Serial.print(F("Purge Vers Reserve Vide : Hum="));
      Serial.println(valeurHumidite);
      return STEP_REMPLISSAGE_FROID_ID;
  }
  */
  return 0;
}

void activatePurgeMachineDansReserve(Step *pStep) {
  pStep->endTime = pStep->startTime + MAX_TEMP_PURGE_DANS_RESERVE;
  isReservePleine = TRUE;
  digitalWrite(PIN_OUT_VALVE_RESERVE, HIGH);
  digitalWrite(PIN_OUT_PURGE, HIGH);
}

String displayPurgeMachineDansReserve() {
    return F("Vers Reserve");
}

String displayPurgeMachineSortie() {
  return F("Vers sortie");
}
