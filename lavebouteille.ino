#include <Arduino.h>

/*
  A FAIRE

  le message d'erreur ne reste pas si on change d'étape
  pouvoir avoir un message d'info plutot qu'un msg d'erreur avec une tempo
  mettre en param les current step

  // format le compteur horaire avec 2 chiffres
  Vers Reserve : l'affichage bug et le compteur affiche 12:75 avec plein de chiffre en dessous
  
  humidité reste à 150/200 qd on est en vidage
  elle est plutot à 100 qd on fait la purge du froid
  3 sec déclenche la purge complete mais aussi le step par defaut
  Il y a une fuite sur le coté gauche (pompe aux) 

  Perte d'eau vers la purge.
  S'affole qd on a stopError
*/
/*

  remplissage | remplissage depuis reserve | purge complete
  |
  eau chaude
  |
  purge reserve | purge sortie
  |
  remplissage eau froide
  |
  eau froide
  |
  purege apres lavage (purge)
  |
  vidage
  |
  remplissage | remplissage depuis reserve | purge complete (appui boutton)

  Ouverture à tout moment puis reprise
  Affichage des erreurs eventuelles

  A faire

  les erreurs
  ouverture de la porte

  bouton

  1 appui bouton : arret bouton temporaire
  1 appui prolongé 3 sec : arret complet et déclenchement de la purge full.
*/

int FALSE = 0;
int TRUE = 1;

const unsigned long MAX_TEMP_PURGE_RESERVE = 60000;
const float TEMP_EAU_CHAUDE = 65.0;
const unsigned long MAX_TEMP_CYCLAGE_CHAUD = 600000;
const unsigned long MAX_TEMP_CYCLAGE_FROID = 90000;
const unsigned long MAX_TEMP_PURGE = 30000;
const unsigned long MAX_TEMP_PURGE_COMPLETE = 120000;
const unsigned long MAX_TEMP_PURGE_DANS_RESERVE = 60000;

//Arret en micro second à chaque cycle arduino
const int MS_DELAY = 100;

//Nombre de cycle arduino ou un appui bouton déclenche l'arret complet
//Dépend de MS_DELAY
const int NB_CYCLE_ARRET_COMPLET = 10;


// 2 min
const unsigned long ERR_MAX_TEMP_REMPLISSAGE = 120000;

const int MAX_COUNT_UTILISATION_RESERVE = 3;

#ifdef __arm__
// should use uinstd.h to define sbrk but Due causes a conflict
extern "C" char* sbrk(int incr);
#else  // __ARM__
extern char *__brkval;
#endif  // __arm__

int freeMemory() {
  char top;
#ifdef __arm__
  return &top - reinterpret_cast<char*>(sbrk(0));
#elif defined(CORE_TEENSY) || (ARDUINO > 103 && ARDUINO != 151)
  return &top - __brkval;
#else  // __arm__
  return __brkval ? &top - __brkval : &top - __malloc_heap_start;
#endif  // __arm__
}
